export interface PostMessage {
  id: number
  userId: number
  title: string
  body: string
}

export interface UserMessage {
  id: number
  name: string
  username: string
  email: string
  address: object
}

export type UserPostMessage = PostMessage & {
  userName: string
  userEmail: string
}

export const FETCH_POSTS = "FETCH_POSTS"
export const FETCHING_POSTS = "FETCHING_POSTS"
export const FETCH_USERS = "FETCH_USERS"
export const FETCH_USERS_POSTS = "FETCH_USERS_POSTS"
