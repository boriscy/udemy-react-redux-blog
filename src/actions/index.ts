import {
  FETCH_POSTS,
  FETCH_USERS,
  FETCH_USERS_POSTS,
  UserMessage,
  PostMessage,
} from "actions/types"
//import jsonPlaceholderAPI from "apis/jsonplaceholder" NOT WORKING CORRECTLY
import axios from "axios"
import { Dispatch } from "redux"
import { UserPostMessage } from "actions/types"

export const addPosts = (posts: [] = []) => ({
  type: FETCH_POSTS,
  payload: posts,
})

export const addUsers = (users: [] = []) => ({
  type: FETCH_USERS,
  payload: users,
})

interface UsersObjMessage {
  [field: string]: UserMessage
}

function createUsersObj(users: UserMessage[]): UsersObjMessage {
  let usersObj: UsersObjMessage = {}
  users.forEach((user) => {
    usersObj[`${user.id}`] = user
  })

  return usersObj
}

export function postWithUsers(posts: PostMessage[], users: UserMessage[]) {
  const usersObj = createUsersObj(users)
  return posts.map((post) => {
    const user = usersObj[`${post.userId}`]
    return { ...post, userName: user.name, userEmail: user.email }
  })
}

export const addUsersAndPosts = (usersPosts: UserPostMessage[] = []) => ({
  type: FETCH_USERS_POSTS,
  payload: usersPosts,
})

export const fetchPostsAndUsers = () => {
  return async (dispatch: Dispatch<any>, getState: Function) => {
    await dispatch(fetchPosts())
    //const posts = getState().posts
    await dispatch(fetchUsers())
    //const users = getState().users
    //const data = postWithUsers(posts, users)
    //dispatch(addUsersAndPosts(data))
  }
}

export const fetchPosts = () => {
  return async function (dispatch: Dispatch) {
    try {
      const response = await axios.get(
        "https://gist.githubusercontent.com/boriscy/6b407e4581ac62df0118c5619e5416dc/raw/12a2c3aa7f9b015b2232b675a8a37b6269b92dc9/posts.json"
      )
      dispatch(addPosts(response.data))
    } catch (err) {
    } finally {
    }
  }
}

export const fetchUsers = () => {
  return async function (dispatch: Dispatch) {
    const response = await axios.get(
      "https://gist.githubusercontent.com/boriscy/6b407e4581ac62df0118c5619e5416dc/raw/12a2c3aa7f9b015b2232b675a8a37b6269b92dc9/users.json"
    )
    dispatch(addUsers(response.data))
  }
}
