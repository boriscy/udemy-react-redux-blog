import React from "react"
import { connect } from "react-redux"
import { ThunkDispatch } from "redux-thunk"
import { fetchPostsAndUsers } from "actions"
import { UserPostMessage } from "actions/types"
import UserHeader from "components/UserHeader"

interface State {}

interface PostListProps {
  posts: UserPostMessage[]
}

interface DispatchProps {
  fetchPostsAndUsers: typeof fetchPostsAndUsers
}

type Props = PostListProps & DispatchProps

class PostList extends React.Component<Props, State> {
  componentDidMount() {
    this.props.fetchPostsAndUsers()
  }

  renderList() {
    return this.props.posts.map((post) => {
      return (
        <div className="item" key={post.id}>
          <i className="icon user aligned middle large" />
          <div className="content">
            <h2>{post.title}</h2>
            <p>{post.body}</p>
          </div>
          <UserHeader userId={post.userId} />
        </div>
      )
    })
  }

  render() {
    return <div className="ui relaxed divided list">{this.renderList()}</div>
  }
}

const mapStateToProps = (state: { posts: any }) => {
  return { posts: state.posts }
}

const mapDispatchToProps = (
  dispatch: ThunkDispatch<{}, {}, any>
): DispatchProps => {
  return {
    fetchPostsAndUsers: () => dispatch(fetchPostsAndUsers()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostList)
