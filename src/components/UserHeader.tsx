import React, { Component } from "react"
import { connect } from "react-redux"
import { UserMessage } from "actions/types"

interface UserHeaderProps {
  user: any
}

class UserHeader extends Component<UserHeaderProps> {
  render() {
    const { user } = this.props
    if (!user) {
      return null
    }
    return <div className="header">{user.name}</div>
  }
}

const mapStateToProps = (state: { users: UserMessage[] }, ownProps: any) => {
  return { user: state.users.find((user) => user.id === ownProps.userId) }
}

export default connect(mapStateToProps)(UserHeader)
