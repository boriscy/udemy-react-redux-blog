import { FETCH_USERS, UserMessage } from "actions/types"

interface Action {
  type: string
  payload: UserMessage[]
}

const initState: UserMessage[] = []

const fetchUsersReducer = (state = initState, action: Action) => {
  switch (action.type) {
    case FETCH_USERS:
      return action.payload
    default:
      return state
  }
}

export default fetchUsersReducer
