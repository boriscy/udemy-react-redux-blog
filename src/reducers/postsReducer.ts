import {
  FETCH_POSTS,
  PostMessage,
  FETCH_USERS_POSTS,
  UserPostMessage,
} from "actions/types"

interface Action {
  type: string
  payload: PostMessage[] | UserPostMessage[]
}

const initState = [
  {
    userId: 1,
    id: 1,
    title:
      "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    body:
      "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
  },
]

export const fetchPostsReducer = (state = initState, action: Action) => {
  switch (action.type) {
    case FETCH_POSTS:
      return action.payload
    case FETCH_USERS_POSTS:
      return action.payload
    default:
      return state
  }
}

export default fetchPostsReducer
